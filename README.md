# Atom configuration

* Install Atom 1.3.*
* Quit Atom
* Go to the following folder (on Mac OS X) : `~/.atom`
* Clone the content of the repository into it : `git clone https://gitlab.com/florian.dwm/my-atom-config.git .`
* Open Atom and enjoy !

# Installed packages

* aligner
* atom-beautify
* codeigniter
* emmet
* pigments
* simple drag drop text